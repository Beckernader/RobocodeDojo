package sample;
import robocode.*;
import robocode.HitRobotEvent;
import robocode.Robot;
import robocode.ScannedRobotEvent;
import robocode.HitByBulletEvent;
import java.awt.*;
//import java.awt.Color;

// API help : http://robocode.sourceforge.net/docs/robocode/robocode/Robot.html

/**
 * FoggyNelson - a robot by (your name here)
 */
public class FoggyNelson extends AdvancedRobot{
	
 	
   public void run() {  
	  //Definem as cores do robô	
      setBodyColor(Color.green);
	  setGunColor(Color.blue);
	  setRadarColor(Color.blue);
	  setScanColor(Color.blue);
	  
 
	  while(true){	
	     turnRadarRight(360);
	     ahead(100);
	     turnGunRight(360);
	     back(100);
       	 
	  }
   }
   //Detecta os outros robôs
   public void onScannedRobot(ScannedRobotEvent e) {
      double max = 200;
	 
	  //Obtém os nomes do Time para evitar o fogo amigo.
	  
	 e.getName().equals("Borderguard");
        doNothing();
     e.getName().equals("JJJameason");
        doNothing();
     e.getName().equals("HowardTheDuck");
        doNothing();
	
	  //Faz um controle da energia que é gasta no que diz 
	  //respeito à potência do tiro
      if(e.getEnergy() < max){
         max = e.getEnergy();
         miraCanhao(e.getBearing(), max, getEnergy());
      }else if(e.getEnergy() >= max){
         max = e.getEnergy();
         miraCanhao(e.getBearing(), max, getEnergy());
      }else if(getOthers() == 2){
         max = e.getEnergy();
         miraCanhao(e.getBearing(), max, getEnergy());
      }
    }
    //quando o seu robo colide com outro robo
    public void onHitRobot(HitRobotEvent e) {
	   tiroFatal(e.getBearing(), e.getEnergy(), getEnergy());	
	
    }
    //Quando seu roboô leva um tiro
    public void onHitByBullet(HitByBulletEvent e) { 
	   turnLeft(100);
	   back(100); 
    }
    //Fornece as coordenadas para o ajuste do canhão.
    public void miraCanhao(double PosIni, double energiaIni, double minhaEnergia) {
       double Distancia = PosIni;
	     double Coordenadas = getHeading() + PosIni - getGunHeading();
	     double PontoQuarenta = (energiaIni / 4) + .1;
            if (!(Coordenadas > -180 && Coordenadas <= 180)) {
                while (Coordenadas <= -180) {
                Coordenadas += 360;
		  }
		  while (Coordenadas > 180) {
		     Coordenadas -= 360;
		  }
	   }
	   turnGunRight(Coordenadas);
		
	   if (Distancia > 200 || minhaEnergia < 15 || energiaIni > minhaEnergia){
          fire(1);
                  } else if (Distancia < 30 ) {
                      fire(3);
                            } else {
                                fire(PontoQuarenta);
       }
   }
                //É chamado quando o robô bate na parede,
                         public void onHitWall(HitWallEvent e) {
                              turnLeft(100);
                              ahead(200);
   }
   

   public void tiroFatal(double PosIni, double energiaIni, double minhaEnergia) {
      double Distancia = PosIni;
	  double Coordenadas = getHeading() + PosIni - getGunHeading();
	  double PontoQuarenta = (energiaIni / 4) + .1;
	  if (!(Coordenadas > -180 && Coordenadas <= 180)) {
	     while (Coordenadas <= -180) {
            Coordenadas += 360;
            fire(2);
		 }
		 while (Coordenadas > 180) {
	        Coordenadas -= 360;
	     }
	  }
	  turnGunRight(Coordenadas);
	  fire(PontoQuarenta);
       
   }
			
}
