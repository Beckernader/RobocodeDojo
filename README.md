# Disciplina de Engenharia de Software II / Laboratório de Programação I: d:RobocodeDojo

Repositório criado para o trabalho das disciplinas Engenharia de Software II / Laboratório de Programação I envolvendo Robocode.

## 1. Das diretrizes gerais de desenvolvimento

- O algoritmo de cada robô deve ser distinto.

- Os robôs devem se manter a uma distância mínima das bordas (ex: 50px).

- Cada robô deve ter um nome derivado de personagens da MARVEL: `Robo JJJameson`, `Robo FoggyNelson` e `Robo HowardTheDuck`,
sendo "Roboticx" o prefixo que indica o nome da equipe. Antes de atirar deve-se checar se o robô scanneado não é do time,
de modo a se evitar *friend fire*.

- Os robôs da equipe competirão contra os robôs `sample.Walls`, `sample.Tracker` e `sample.SpinBot`, e posteriormente contra
os das demais equipes. É importante que estes sejam incluidos em todas as etapas de teste. 

## 2. Do desenvolvimento e implementação

- Utilizar a versão 1.9.3 do Robocode.

- O código pode ser editado utilizando qualquer IDE ou editor a cargo do desenvolvedor, e compilado em um arquivo .JAR
com uso do NetBeans (incluindo-se as devidas bibliotecas) ou com o próprio compilador do Robocode.

- O antes de cada commit é importante checar o comportamento do robô editado no Robocode, e se este está sendo apto a vencer dos robôs `sample.Walls`, `sample.Tracker` e `sample.SpinBot` em pelo menos 4 partidas de 10 rounds. 

## 3. Da equipe

- A equipe é composto pelos Engenheiros Marciel Becker, Patrick Gonçalves e Wellington Sola, também compõe a equipe a programadora Tainá Leivas.

## 4. Das issues e milestones

- Os Engenheiros de Software definirão milestones com prazo de uma semana, sendo estas compostas por uma ou
mais issues, com peso definido e equilibrado, alocadas para cada Programador.

- O prazo de solução das issues para solução das issues deve ser respeitado, ficando o Programador sujeito a
desligamento da equipe em caso de descumprimento não justificado ou reincidente. 

- Os Engenheiros de Software reportarão semanalmente o andamento do projeto ao professor Angelo.

## 5. Dos robôs

- `Robo JJJameson` deve se manter mais próximo ao centro da arena, atirar sempre que um inimigo for detectado, movendo-se para para um lado aleatório (entre 0 e 360) e uma distância de 30 das bordas. Se estiver com uma distância maior de 200 do centro, 
deve voltar, e esta checagem deve ser feita ao final de cada ciclo do loop. 

- `Robo FoggyNelson` deve se manter entre o centro e as extremidades da arena, mantendo sempre a distância
maior que 50 da borda. Em caso de um inimigo detectado deve-se atirar com maior intensidade (fire > 3), mas apenas
se a sua própria energia não estiver baixa.

- `Robo HowardTheDuck` caminhar aleatoriamente pela arena, mantendo uma distância mínima da borda (15px ou mais), e atirar com
alta intensidade em caso de rastrear inimigos com baixa vida. Caso contrário, atirar com fire = 1. Evitar sempre as bordas.

- Todos os robôs devem ter uma variavel privada `turnValue` que controla quantos graus ele rotacionará o "Tanque"
a cada volta do loop no sentido *left* (`turnGunLeft`). Este valor deve ser diminuiu o invertido (ex: `turnValue = -1 * turnValue`)
quando um inimigo for detectado.
