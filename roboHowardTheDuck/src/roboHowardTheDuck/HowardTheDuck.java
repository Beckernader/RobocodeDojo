package roboHowardTheDuck;

import java.awt.Color;

import robocode.*;
import robocode.util.Utils;

public class HowardTheDuck extends AdvancedRobot {
	int dist = 50; //cria uma distância padrão para movimentação
	
	public void run() {
		
		//Definição das cores do robo
		
		setBodyColor(Color.green);
		setGunColor(Color.blue);
		setScanColor(Color.blue);
		setRadarColor(Color.orange);
		
		while (true) {
	
			//Ajusta a arma e o radar do robo a cada inicio de turno.		
			
			setAdjustGunForRobotTurn(true);
			setAdjustRadarForRobotTurn(true);
			setTurnRadarRight(Double.POSITIVE_INFINITY);
			
			//Movimentação livre do robo
			
			 turnRadarRight(10);
			 turnGunRight(10);
			 setAhead(100);
			 setTurnLeft(75);
		     setBack(100);
		     setTurnRight(75);
			
			this.execute(); 
					
		}
		
	}
	
	//Detecta outros robos 
	public void onScannedRobot(ScannedRobotEvent enemy) {
		double min = 100;	//movimentação minima ao detectar um robo
	
		//Procura "travar" a mira no inimigo
	setTurnRadarRight(2.0 * Utils.normalRelativeAngleDegrees(getHeading() +
			enemy.getBearing() - getRadarHeading())); 
	
	//Caso o inimigo esteja muito perto é usado o fire 3
	if (enemy.getDistance() >= 50 && getEnergy() > 50) {
		fire(3);
		} // caso contrário, atira com intensidade 1.
		else {
		fire(1);
		}
	
	    scan(); //chama o scan para detectar os outros robos novamente

	//Obtém os nomes determinados, assim evitando o fogo amigo.
	 
      enemy.getName().equals("FoggyNelson");
     doNothing();
     enemy.getName().equals("JJJameson");
	 doNothing();
	  
	 //Controla a energia do tiro
     
		if(enemy.getEnergy() < min){
	         min = enemy.getEnergy();
	         miraArma(enemy.getBearing(), min, getEnergy());
	      }else if(enemy.getEnergy() >= min){
	         min = enemy.getEnergy();
	         miraArma(enemy.getBearing(), min, getEnergy());
	      }else if(getOthers() == 2){
	         min = enemy.getEnergy();
	         miraArma(enemy.getBearing(), min, getEnergy());
	      }else if (getEnergy() < 30);
	       doNothing();  //Caso a energia esteja menor do que 30, ele ignora sua vez no turno
	}

	
	//Quando o robo colidir com outro robo --
	//A variável turnGumAmt recebe o valor do calculo
    //de quanto a mira deve ser ajustada.
    //Para encontrar o valor adequado de ajuste, essa função é chamada
    //para normalizar um ângulo. 

	public void onHitRobot(HitRobotEvent enemy) {
		double turnGunAmt = anguloRelativo(enemy.getBearing() + 
				getHeading() - getGunHeading());
				turnGunLeft(turnGunAmt);
				fire(3);
	}
	
	//Quando for atingido por um disparo, ele gira perdicularmente à bala e avança um pouco.
	
	public void onHitByBullet(HitByBulletEvent e) { 
		turnRight(anguloRelativo(90 - (getHeading() - 
				e.getHeading()))); /* o robo gira para a direita de acordo com o angulo que ele foi	atingido*/
		
		double dist = 35; //variável que define a distancia de movimentação
		
		setAhead(dist); //O robo se movimentara de acordo a quantidade definida dentro da variável
		dist *= -1;
		scan();
	    }

	private double anguloRelativo(double ANG) { //Método responsável pelo calculo quando o robo for 
		                                         //atingido por uma bala

        if (ANG> -180 && ANG <= 180) {
        	return ANG;
        }
		double REL = ANG;
		while (REL<= -180) {
			REL += 360;
		}
		while (ANG> 180) {
			REL -= 360;
		}
		return REL;
	}

	//Definições de ajustes para a mira do robo
	  public void miraArma(double PosEnemy, double energiaEne, double myEnergy) {
	        
		     double Distancia = PosEnemy;
		     double Coordenadas = getHeading() + PosEnemy - getGunHeading();
		     double Canhao = (energiaEne / 4) + .1;
	            if (!(Coordenadas > -180 && Coordenadas <= 180)) {
	                while (Coordenadas <= -180) {
	                Coordenadas += 360;
			  }
			  while (Coordenadas > 180) {
			     Coordenadas -= 360;
			  }
		   }
		   turnGunRight(Coordenadas);
			
		   if (Distancia > 200 || myEnergy < 15 || energiaEne > myEnergy){
	         setFire(1);
	                  } else if (Distancia < 35 ) {
	                      setFire(3);
	                            } else {
	                                setFire(Canhao);
	       }
	   }
	    
	  //É acionado quando o robô colidir com a parede,
	       public void onHitWall(HitWallEvent e) {
	       setBack(200);
	       setTurnLeft(90);    //recuo de 200 pixels
	   }
	   

	   public void tiroFatal(double PosEnemy, double energiaEne, double minhaEnergia) {
		 
	      double Coordenadas = getHeading() + PosEnemy - getGunHeading();
		  double Bazuca = (energiaEne / 4) + .1;
		 
		  if (!(Coordenadas > -180 && Coordenadas <= 180)) {
		     while (Coordenadas <= -180) {
		        Coordenadas += 360;
			 }
			 while (Coordenadas > 180) {
		        Coordenadas -= 360;
		     }
		  }
		  turnGunRight(Coordenadas);
		  setFire(Bazuca);
	       
	}
	   public void onWin(WinEvent enemy) {
			for (int i = 0; i < 50; i++) {
				turnRight(40);
				turnLeft(40);
			}
      }
}



