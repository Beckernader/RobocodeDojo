package roboJameson;

import java.awt.Color;
import robocode.*;
import robocode.util.Utils;

public class JJJameson extends AdvancedRobot {
       int distancia = 50; //Distancia padrão de movimentação do robo
       
	public void run() {
	
		//Definição das cores do robo
		
		setBodyColor(Color.green);
		setGunColor(Color.blue);
		setRadarColor(Color.blue);
		
		while (true) {
	
			//Ajusta a arma e o radar do robo a cada inicio de turno.		
			
			setAdjustGunForRobotTurn(true);
			setAdjustRadarForRobotTurn(true);
			setTurnRadarRight(Double.POSITIVE_INFINITY);
	
			//Movimentação padrão do robo	
	
			 turnRadarRight(10);
			 turnGunRight(10);
			 setAhead(50);
		     setBack(50);
			
			this.execute(); 
					
		}
		
	}
	
	//Detecta outros robos 
	public void onScannedRobot(ScannedRobotEvent enemy) {
		double min = 100;	
		
	setTurnRadarRight(2.0 * Utils.normalRelativeAngleDegrees(getHeading() +
			enemy.getBearing() - getRadarHeading()));

	//Obtém os nomes determinados, assim evitando o fogo amigo.
	 
      enemy.getName().equals("FoggyNelson");
     doNothing();
     enemy.getName().equals("HowardTheDuck");
	 doNothing();
	  
	 //Controle de energia que sera gasta 
     
		if(enemy.getEnergy() < min){
	         min = enemy.getEnergy();
	         miraArma(enemy.getBearing(), min, getEnergy());
	      }else if(enemy.getEnergy() >= min){
	         min = enemy.getEnergy();
	         miraArma(enemy.getBearing(), min, getEnergy());
	      }else if(getOthers() == 2){
	         min = enemy.getEnergy();
	         miraArma(enemy.getBearing(), min, getEnergy());
	      }else if (getEnergy() < 30);
	       doNothing();
	}



	//Quando o robo colidir com um robo 
	public void onHitRobot(HitRobotEvent enemy) {
		 tiroFatal(enemy.getBearing(), enemy.getEnergy(), getEnergy()); //Atira no robo inimigo com força maxima
		 
		 //Após o tiro, move-se para longe
		 setTurnLeft(90);
		 setAhead(50);
	}
	
	//Quando o robo for acertado por um tiro
	public void onHitByBullet(HitByBulletEvent e) { 
		   turnLeft(90);
		   back(70); 
	    }
	
	  //É acionado quando o robô colidir com a parede,
    public void onHitWall(HitWallEvent e) {
    turnRight(90);
    ahead(200);
      }
   
    
    /*Obtém a energia do inimigo e posição
     * calcula as coordenadas de acordo com as informações adquiridas
     * e em seguida gira a arma na direção da coordenada que se tem
     */
	   public void tiroFatal(double PosEnemy, double energiaEne, double minhaEnergia) {
		 
	      double Coordenadas = getHeading() + PosEnemy - getGunHeading();
		  double Bazuca = (energiaEne / 4) + .1;
		 
		  if (!(Coordenadas > -180 && Coordenadas <= 180)) {
		     while (Coordenadas <= -180) {
		        Coordenadas += 360;
			 }
			 while (Coordenadas > 180) {
		        Coordenadas -= 360;
		     }
		  }
		  turnGunRight(Coordenadas);
		  setFire(Bazuca);
	       
	} 
    
	//Ajuste da mira do robo e definições dos tiros
	  public void miraArma(double PosEnemy, double energiaEne, double myEnergy) {
	        
		     double Distancia = PosEnemy; //Posição do inimigo
		     double Coordenadas = getHeading() + PosEnemy - getGunHeading(); //Obtém as coordenadas do inimigo
		     double Bazuca = (energiaEne / 4) + .1;    // Tiro fatal, definição da ação do robo
	            if (!(Coordenadas > -180 && Coordenadas <= 180)) { 
	                while (Coordenadas <= -180) {
	                Coordenadas += 360;
			  }
			  while (Coordenadas > 180) {
			     Coordenadas -= 360;
			  }
		   }
	            
		   turnGunLeft(Coordenadas); //Gira a arma na direção das coordenadas obtidas
			
		   // Faz a checagem para saber qual dos fires será usado
		   
		   if (Distancia > 200 || myEnergy < 15 || energiaEne > myEnergy){    
	         setFire(1);
	                  } else if (Distancia < 35 ) {
	                      setFire(3);
	                            } else {
	                                setFire(Bazuca);
	       }
	   }
 
	   public void onWin(WinEvent enemy) {
			for (int i = 0; i < 50; i++) {
				turnRight(40);
				turnLeft(40);
			}
      }
}
